# Nginx with folder vigilance

This Docker image provides a ready-to-use environment for nginx.

This script monitors the directories specified in the **DIRECTORIES** variable for any changes using inotifywait. If any changes occur, it waits for 10 minutes (600 seconds) before reloading the nginx configuration using **kill -HUP ${NGINX_PID}** reload. 

## Usage

### Via compose

```yml
version: "3"

services :
  nginx:
    image: pepramon/nginx
    volumes:
      # Tipical volume to configure nginx
      - ./conf.d:/etc/nginx/conf.d
    environment:
      # Example to monitor 2 directories
      DIRECTORIES=/etc/example /var/www
```

### Run command

Same as docker-compose but with a command

```bash
docker run -d -v ./conf.d:/etc/nginx/conf.d -e DIRECTORIES="/etc/example /var/www"  pepramon/nginx
```

## Support and Contributions

For support or to contribute to the development of this project, please visit the official repository on GitLab: [https://gitlab.com/imagenes-docker/nginx](https://gitlab.com/imagenes-docker/nginx).

## Author

This project was created by Josep Ramón Martinez.