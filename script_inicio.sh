#!/bin/bash

# For clean exit
clean_exit() {
    echo "Shutting down"
    
    # Kill directory monitoring
    if [ ${#PID_DIRS[@]} -gt 0 ]; then
        for pid in "${PID_DIRS[@]}"; do
            kill -SIGTERM "$pid" 2>/dev/null
            wait "$pid" 2>/dev/null
        done
    fi
    
    # Clear temp files
    echo "Cleaning"
    rm -f /tmp/nginx_reload_*_lock
    
    # Kill nginx
    if kill -0 "${NGINX_PID}" 2>/dev/null; then
        kill -SIGTERM "${NGINX_PID}"
        wait "${NGINX_PID}"
    fi
    exit
}

trap "exit" TERM INT QUIT
trap "clean_exit" EXIT SIGINT SIGTERM

# Launch nginx with his parameters
/docker-entrypoint.sh "$@" &
NGINX_PID=$!

execute_reload_once() {
    # Lock file to have tieme to modify files
    local dir="$1"
    # Lock per dir
    local lock_file="/tmp/nginx_reload_${dir//\//-}_lock"

    if [ ! -f "$lock_file" ]; then
        touch "$lock_file"
        sleep 600s  # Wait to be sure all modificatios will done
        echo "Reloading nginx"
        kill -HUP ${NGINX_PID}
        rm "$lock_file"
    fi
}

# Vigilancia de los directorios
declare -A PID_DIRS
for DIR in $DIRECTORIES; do
    if [ -d "$DIR" ]; then
        (
        while true; do
            inotifywait -m -r -e modify,create,delete,move "$DIR" | 
            while read -r directory event filename; do
                execute_reload_once "$DIR" &
            done
        done
        ) &
        PID_DIRS[$DIR]=$!
    else
        echo "$DIR no exist."
        sleep 5s
        exit 1
    fi
done

if [ ${#PID_DIRS[@]} -gt 0 ]; then
    wait -n "${NGINX_PID}" "${PID_DIRS[@]}"
    EXIT_CODE=$?
else
    wait -n "${NGINX_PID}"
    EXIT_CODE=$?
fi

echo "Exit with code ${EXIT_CODE}"
exit ${EXIT_CODE}
