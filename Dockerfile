# Utilizamos la imagen oficial de certbot como base
FROM nginx:stable

# Autor del Dockerfile
LABEL maintainer="Josep Ramón Martinez (perfil gitlab o github, pepramon)"

# Copiar el script de inicio al contenedor
COPY script_inicio.sh /script_inicio.sh

# For inotify
RUN apt-get update && \
    apt-get install -y --no-install-recommends inotify-tools && \
    apt autoremove -y && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* 

# Definir el script como entrypoint
ENTRYPOINT ["/script_inicio.sh"]
CMD ["nginx", "-g", "daemon off;"]
